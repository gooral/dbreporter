package configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import jdbcReporter.JdbcReporter;
import utils.CustomLogger;

public class Environments {

	public static String DB_URL = "";
	public static String USER = "";
	public static String PASS = "";

	public ArrayList<String> envs = new ArrayList<>();

	/*function used to load configured environments names out of environemntsConfiguration.txt file
	the file need to provide statements in following way:
	- every environment in separate line
	- format: <ENVIRONMENTNAME>|<DB_URL>:<PORT NUMBER>:<SSID>|<USERNAME>|<PASSWORD>
	this function uses <ENVIRONMENT> to build up combobox*/
	public void loadEnvironmentsNames() {
		File file = new File("./environmentsConfiguration.txt");

		String line;

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file.getPath())); //load file into file reader
			try {
				while ((line = reader.readLine()) != null) {
					String[] parts = line.split("\\|", 4);
					if (parts.length == 4) {
						envs.add(parts[0]); // populate public array 'envs' with first parts of configuration file records - environemnts name
					}
				}
				reader.close();
			} catch (IOException e) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "ERROR WHILE READING ENVIRONMENT CONF FILE", "Error",
						JOptionPane.ERROR_MESSAGE);
				CustomLogger.error("ERROR WHILE READING FILE", e);
			}
		} catch (FileNotFoundException e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "ENV CONF FILE NOT FOUND", "Error", JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("FILE NOT FOUND", e);
		}
	}
	
	/*function used to load configured environments names out of environemntsConfiguration.txt file
	the file need to provide statements in following way:
	- every environment in separate line
	- format: <ENVIRONMENTNAME>|<DB_URL>:<PORT NUMBER>:<SSID>|<USERNAME>|<PASSWORD>
	this function uses <DB_URL>:<PORT NUMBER>:<SSID>|<USERNAME>|<PASSWORD> to populate connection details*/
	public void loadEnvironmentData() {
		File file = new File("./environmentsConfiguration.txt");

		String line;

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file.getPath()));
			try {
				while ((line = reader.readLine()) != null) {
					String[] parts = line.split("\\|", 4);
					if (parts[0].equals(JdbcReporter.environment)) {
						DB_URL = parts[1].toString();
						USER = parts[2].toString();
						PASS = parts[3].toString();
					}
				}
				reader.close();
			} catch (IOException e) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "ERROR WHILE READING FILE", "Error", JOptionPane.ERROR_MESSAGE);
				CustomLogger.error("ERROR WHILE READING ENVIRONMENT CONF FILE", e);
			}
		} catch (FileNotFoundException e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "ENV CONF FILE NOT FOUND", "Error", JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("FILE NOT FOUND", e);
		}
	}
	
	public String getDBurl() {
		return DB_URL; //Method used to pass the DB_URL to the connection manager
	}
	
	public String getUser() {
		return USER; //Method used to pass the USER to the connection manager
	}
	
	public String getPass() {
		return PASS; //Method used to pass the PASSWORD to the connection manager
	}

}