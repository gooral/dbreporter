package configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import utils.CustomLogger;
import utils.StatementProcessor;

public class SQLStatements {
	
	TreeMap<String, String> statements = new TreeMap<String, String>(); //statements TreeMap used to store all the SQL queries loaded from statements.txt
	
	/*Process statements function is used to load SQL queries from statements.txt and if estraStatements.txt conf files, 
	 * which are stored in <Key, Value manner with delimiter '|'*/
	public void processStatements(boolean includeExtraStatements) {

		File stmts = new File("./statements.txt"); //load conf file file
		
		String line;

		try {
			BufferedReader reader = new BufferedReader(new FileReader(stmts.getPath()));
			try {
				while ((line = reader.readLine()) != null) {
					String[] parts = line.split("\\|", 2);
					if (parts.length >= 2) {
						String key = parts[0];
						String value = parts[1];
						statements.put(key, value);
					}
				}
				reader.close();
			} catch (IOException e) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "ERROR WHILE READING FILE", "Error",
						JOptionPane.ERROR_MESSAGE);
				CustomLogger.error("ERROR WHILE READING FILE", e);
			}
		} catch (FileNotFoundException e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "FILE NOT FOUND", "Error",
					JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("FILE NOT FOUND", e);
		}
		

		if (includeExtraStatements == true) {

			File extraStmts = new File("./extraStatements.txt");

			try {
				BufferedReader reader = new BufferedReader(new FileReader(extraStmts.getPath()));
				try {
					while ((line = reader.readLine()) != null) {
						String[] parts = line.split("\\|", 2);
						if (parts.length >= 2) {
							String key = parts[0];
							String value = parts[1];
							statements.put(key, value);
						}
						
					}
					reader.close();
				} catch (IOException e) {
					final JPanel panel = new JPanel();
					JOptionPane.showMessageDialog(panel, "ERROR WHILE READING FILE", "Error",
							JOptionPane.ERROR_MESSAGE);
					CustomLogger.error("ERROR WHILE READING FILE", e);
				}
			} catch (FileNotFoundException e) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "FILE NOT FOUND", "Error",
						JOptionPane.ERROR_MESSAGE);
				CustomLogger.error("FILE NOT FOUND", e);
			}

		}
		
		//Instruction that invoke statement processor
		StatementProcessor processor = new StatementProcessor();
		
		//Loop going through statements tree map, processing each query through the database into HTML output, with HTML section for each
		for (Map.Entry<String, String> entry : statements.entrySet()) {
			
			CustomLogger.startSection(entry.getKey());
			processor.executeStatement(entry.getValue());
			CustomLogger.endSection(entry.getValue().toString());

		}
	}
}