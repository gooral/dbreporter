package jdbcReporter;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import configuration.Environments;
import utils.CustomLogger;
import utils.InputValidator;

public class MainFrame {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {
		initialize();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() {

		Environments conf = new Environments();
		conf.loadEnvironmentsNames();

		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 500, 250);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setTitle("dbReporter by Mateusz Goral");
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		JLabel lblNewLabel = new JLabel("Select environment:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		frame.getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 0);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 1;
		frame.getContentPane().add(comboBox, gbc_comboBox);
		comboBox.setModel(new DefaultComboBoxModel(conf.envs.toArray()));

		JLabel lblNewLabel_1 = new JLabel("Provide input:");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		frame.getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		JTextField textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 3;
		frame.getContentPane().add(textField, gbc_textField);

		JCheckBox chckbxIncludeExtraStatements = new JCheckBox("Include extra statements?");
		chckbxIncludeExtraStatements.setSelected(false);
		GridBagConstraints gbc_chckbxIncludeExtraStatements = new GridBagConstraints();
		gbc_chckbxIncludeExtraStatements.insets = new Insets(0, 0, 5, 0);
		gbc_chckbxIncludeExtraStatements.gridx = 0;
		gbc_chckbxIncludeExtraStatements.gridy = 5;
		frame.getContentPane().add(chckbxIncludeExtraStatements, gbc_chckbxIncludeExtraStatements);

		JCheckBox chckbxValidateInput = new JCheckBox("Validate input?");
		chckbxValidateInput.setSelected(true);
		GridBagConstraints gbc_checkbxValidateInput = new GridBagConstraints();
		gbc_checkbxValidateInput.insets = new Insets(0, 0, 5, 0);
		gbc_checkbxValidateInput.gridx = 0;
		gbc_checkbxValidateInput.gridy = 4;
		frame.getContentPane().add(chckbxValidateInput, gbc_checkbxValidateInput);

		JButton btnRunScript = new JButton("Run Script");
		GridBagConstraints gbc_btnRunScript = new GridBagConstraints();
		gbc_btnRunScript.insets = new Insets(0, 0, 5, 0);
		gbc_btnRunScript.gridx = 0;
		gbc_btnRunScript.gridy = 6;
		frame.getContentPane().add(btnRunScript, gbc_btnRunScript);
		btnRunScript.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JdbcReporter.environment = comboBox.getSelectedItem().toString();
				JdbcReporter.queryParameter = textField.getText();
				JdbcReporter.includeExtraStatements = chckbxIncludeExtraStatements.isSelected();

				conf.loadEnvironmentData();

				InputValidator validator = new InputValidator();
				JdbcReporter starter = new JdbcReporter();

				if (chckbxValidateInput.isSelected() == true) {
					validator.validateInput(JdbcReporter.queryParameter);
					if (validator.getStatus() == true) {
						try {
							starter.startProgram();
						} catch (IOException e1) {
							final JPanel panel = new JPanel();
							JOptionPane.showMessageDialog(panel, "CANNOT START SCRIPT, CHECK CONFIGURATION FILES",
									"Error", JOptionPane.ERROR_MESSAGE);
							CustomLogger.error("UNKNOWN EXCEPTION", e1);
						}
					} else {
						textField.setText("");
					}
				} else {
					try {
						starter.startProgram();
					} catch (IOException e1) {
						final JPanel panel = new JPanel();
						JOptionPane.showMessageDialog(panel, "CANNOT START SCRIPT, CHECK CONFIGURATION FILES",
								"Error", JOptionPane.ERROR_MESSAGE);
						CustomLogger.error("UNKNOWN EXCEPTION", e1);
					}
				}

			}
		});

	}

}
