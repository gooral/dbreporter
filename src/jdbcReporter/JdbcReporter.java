package jdbcReporter;

import java.io.IOException;

import configuration.SQLStatements;
import utils.CustomLogger;
import utils.OpenReportFile;

public class JdbcReporter {

	public static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
	
	// public variables storing environment and parameters
	public static String environment = "";
	public static String queryParameter = "";
	public static boolean includeExtraStatements = false;

	public void startProgram() throws IOException {
		
		//method starting html report
		CustomLogger.startHTML();
		
		//invocation of method that process all queries
		SQLStatements invoker = new SQLStatements();
		invoker.processStatements(includeExtraStatements);
		
		//end of html report
		CustomLogger.endHTML();
		
		//method opening the successfully created repot and closing the application
		OpenReportFile file = new OpenReportFile();
		file.openFile();

	}
}
