package utils;

import org.apache.log4j.Logger;

import jdbcReporter.JdbcReporter;

public class CustomLogger {
	private static Logger Log = Logger.getLogger(CustomLogger.class.getName());
	

	public static void startHTML() {

		Log.info("<!DOCTYPE�html>");
		Log.info("<html>");
		Log.info("<head>");
		Log.info("<link rel=\nstylesheet\n href=\"https://w3schools.com/w3css/4/w3.css\">");
		
		Log.info("<style>");
		Log.info(".body {background-color: #e6e6e6;}");
		Log.info(".th {font-size: 16px; font-family: \"Times NewRoman\", Times, serif; font-weight: bold; text-align: center;}");
		Log.info(".table {border-collapse: collapse;}");
		Log.info(".table, th, td {border: 0.5pt solid black;}");
		Log.info(".tooltip {position:relative; display:inline-block;} ");
		Log.info(".tooltip .tooltiptext {visibility:hidden; display:block; background-color:black; color:#fff; text-align:center; border-radius:6px; padding:5px 0; position:absolute; z-index:1; top:100%; left:50%; margin-left:-60px;}"); 
		Log.info(".tooltip:hover .tooltiptext {visibility:visible;}");
		Log.info("</style>");
		
		Log.info("<script>");		
		Log.info("function myFunction(id) {var x = document.getElementById(id);if (x.className.indexOf(\"w3-show\") == -1) {x.className += \" w3-show\";} else {x.className = x.className.replace(\" w3-show\", \"\");}}");
		Log.info("</script>");
		
		Log.info("<title>" + JdbcReporter.queryParameter + "-" + JdbcReporter.environment + "</title>"); 
		Log.info("<body>");
	}

	public static void endHTML() {
		Log.info("<div>Script by Mateusz G�ral</div>");
		Log.info("</body>\n" + "</html>");
	}
	
	public static void startSection(String sectionName) {
		Log.info("<button onclick=\"myFunction('" + sectionName + "')\" class=\"w3-block w3-Dark-grey w3-medium w3-padding-large w3-round-xlarge w3-left-align\">");
		Log.info("<b>" + sectionName + "</b></button>");
		Log.info("<div id=\"" + sectionName + "\" class=\"w3-container w3-hide\">");
		
	}
	
	public static void endSection (String query) {
		Log.info("<div class=\"tooltip\"><b>Show query</b><div class=\"tooltiptext\">" + query + "</div></div>");
		Log.info("</div>");	
	}
	
	public static void startTable() {
		Log.info("<div class=\"w3-responsive\">");
		Log.info("<table width=\"100%\" class=\"w3-table-all w3-small w3-hoverable\">");
	}
	
	public static void endTable() {
		Log.info("</table>");
		Log.info("</div>");
	}

	public static void info(String message) {
		Log.info(message);
	}

	public static void warn(String message) {
		Log.warn(message);
	}

	public static void error(String message, Exception e) {
		Log.error("___________ START OF STACKTRACE ___________");
		Log.error(message, e);
		Log.error("___________ END OF STACKTRACE ___________");
	}

	public static void fatal(String message) {
		Log.fatal(message);
	}

	public static void debug(String message) {
		Log.debug(message);
	}

}
