package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.FileAppender;

import jdbcReporter.JdbcReporter;

public class CustomFileAppender extends FileAppender {
	
	public static String path = "";
	
	@Override
	public void setFile(String fileName) {
		if (fileName.indexOf("%timestamp") >= 0) {
			fileName = fileName.replaceAll("%timestamp", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			fileName = fileName.replaceAll("%policyID", JdbcReporter.queryParameter);
			fileName = fileName.replaceAll("%env", JdbcReporter.environment);
			fileName = fileName.replaceAll("%ss", new SimpleDateFormat("ss").format(new Date()));
			path = fileName;
		}
		
		try {
			
			super.setFile(fileName);
			
		} catch (Exception e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,
					"CANNOT CREATE HTML REPORT FILE:  " + CustomFileAppender.path, "Error",
					JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("CANNOT CREATE REPORT FILE", e);
		}
	}

}
