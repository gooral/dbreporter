package utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class OpenReportFile {
	
	public void openFile () {
				
		File reportFile = new File(CustomFileAppender.path);
		
		try {
			Desktop desktop = Desktop.getDesktop();
			desktop.open(reportFile);
			
		} catch (IOException e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel,
					"CANNOT OPEN REPORT FILE:  " + CustomFileAppender.path, "Error",
					JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("CANNOT OPEN FILE\n", e);
		}
		System.exit(0);
	}

}
