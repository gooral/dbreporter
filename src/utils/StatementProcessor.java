package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import configuration.Environments;
import jdbcReporter.JdbcReporter;

public class StatementProcessor {
	
	public void executeStatement(String query) {

		PreparedStatement stmt = null;
		Connection conn = null;

		Environments env = new Environments();

		try {

			Class.forName(JdbcReporter.JDBC_DRIVER);

			conn = DriverManager.getConnection(env.getDBurl(), env.getUser(), env.getPass());
			stmt = conn.prepareStatement(query);
			stmt.setString(1, JdbcReporter.queryParameter);

			ResultSet rs = stmt.executeQuery();

			CustomLogger.startTable();

			List<String> columnNames = new ArrayList<>();
			ResultSetMetaData rsmd = rs.getMetaData();

			CustomLogger.info("<tr>");
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				columnNames.add(rsmd.getColumnLabel(i));
				CustomLogger.info("<th align=\"center\">" + rsmd.getColumnLabel(i) + "</th>");
			}
			CustomLogger.info("</tr>");

			while (rs.next()) {
				CustomLogger.info("<tr>");
				List<Object> rowData = new ArrayList<>();
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					rowData.add(rs.getObject(i));
				}
				for (int colIndex = 0; colIndex < rsmd.getColumnCount(); colIndex++) {
					String objString = "";
					Object columnObject = rowData.get(colIndex);
					if (columnObject != null) {
						objString = columnObject.toString();
					}
					CustomLogger.info("<td>" + objString + "</td>");
				}
				CustomLogger.info("</tr>");
			}

			CustomLogger.endTable();
			rs.close();
			stmt.close();
			conn.close();

		} catch (SQLException se) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "INTERNAL ERROR, CHECK LOGS", "Error", JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("SQL FAIL WRONG QUERY OR ACCESS DATA", se);
		} catch (Exception e) {
			final JPanel panel = new JPanel();
			JOptionPane.showMessageDialog(panel, "INTERNAL ERROR, CHECK LOGS", "Error", JOptionPane.ERROR_MESSAGE);
			CustomLogger.error("UNKNOWN ERROR", e);
		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				final JPanel panel = new JPanel();
				JOptionPane.showMessageDialog(panel, "INTERNAL ERROR, CHECK LOGS", "Error", JOptionPane.ERROR_MESSAGE);
				CustomLogger.error("CANNOT CLOSE CONNECTION", se);
			}
		}

	}

}
