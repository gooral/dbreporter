# README #

dbReporter

### What is this repository for? ###
_____
dbReporter is simple, easily configurable tool with capability of executing batch of single-parameter SQL query on desired database instance and generating html report as output.
Right now it supports ORACLE database.

_____
dbReporter supports easy configuration via txt files with SQL statements and database environments connection data.

### How do I get set up? ###
_____
dbReporter should be compiled into executable jar.

_____
Configuration of SQL statements is done by statements.txt and extraStatements.txt.
extraStatements.txt execution is optional (example: time consuming SQLs can be optionally run)
It is done in following manner:

STATEMENTNAME1|SQLQUERY ( '?' as parameter )
STATEMENTNAM2|SQLQUERY ( '?' as parameter )

Parameters must be the same and statement name will appear as section (table) name in HTML report

Configuration of database access (environments) is done by environmentsConfiguration.txt
It is done in following manner:

ENVNAME|jdbc:oracle:thin:@'IP:PORT:SSID'|USER|PASSWORD

_____
Dependencies:
- ojdbc version 6
- log4j version 1.2.17
- apache commons lang4 version 3.7

### Who do I talk to? ###

Mateusz Góral - mateusz.goral92@gmail.com